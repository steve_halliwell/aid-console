using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;

namespace AID
{
    /*
          Assists with converting the string entered by the user into an array of
          appropriate objects, is used by GConsole extensions. Currently throws 
          during errors.
          */
    public static class StringToType
    {
        public delegate object StringToTypeDelegate(string s);
        public delegate void StringToTypeLogDelegate(string s);

        //Change this to Debug.Log wrapper or perhaps Console.Log wrapper to see conversion errors get logged
        static public StringToTypeLogDelegate logDelegate = (s) => { };

        //these are not very exhaustive
        static Dictionary<System.Type, StringToTypeDelegate> converters = new Dictionary<System.Type, StringToTypeDelegate>()
        {
            {typeof(int)    , (string s) => {return int.Parse(s);} },
            {typeof(float)  , (string s) => {return float.Parse(s);} },
            {typeof(string)  , (string s) => {return s; } },
            {typeof(bool)  , (string s) => {return bool.Parse(s); } },
            {typeof(UnityEngine.Vector4)  , (string s) => { return VectorFromStringFloatArray(s); } },
            {typeof(UnityEngine.Vector3)  , (string s) => { return (UnityEngine.Vector3) VectorFromStringFloatArray(s); } },
            {typeof(UnityEngine.Vector2)  , (string s) => { return (UnityEngine.Vector2) VectorFromStringFloatArray(s); } }
        };

        public static object Convert(System.Type t, string s)
        {
            var f = converters[t];
            return f(s);
        }

        public static UnityEngine.Vector4 VectorFromStringFloatArray(string s)
        {
            return new UnityEngine.Vector4().FromFloatArray(StringToFloatArray(s));
        }

        public static float[] StringToFloatArray(string s)
        {
            Regex regex = new Regex(@"-?(?:\d*\.)?\d+");
            MatchCollection matches = regex.Matches(s);
            float[] res = new float[matches.Count];
            for (int i = 0; i < res.Length; i++)
            {
                try
                {
                    res[i] = float.Parse(matches[i].Value);
                }
                catch (Exception)
                {
                }
            }
            return res;
        }

        //static char[] delimiterChars = { ',' };

        //very stupid and simple now but later may need to support heirarchy 
        public static string[] ParamStringToElements(string s)
        {
            //if (string.IsNullOrEmpty(s))
            //    return new string[0];

            //return s.Split(delimiterChars);

            //thx http://regexr.com/
            Regex regex = new Regex(@"\(.*?\)|\[.*?\]|"".*?""|[^\s]+");
            MatchCollection matches = regex.Matches(s);
            string[] res = new string[matches.Count];
            for (int i = 0; i < res.Length; i++)
            {
                res[i] = matches[i].Value;
            }
            return res;
        }

        //returns false if any fail
        public static bool StringArrayToObjects(string[] sParams, ParameterInfo[] pList, out object[] out_params)
        {
            bool hasSucceded = true;
            out_params = new object[pList.Length];

            if (sParams.Length != pList.Length)
            {
                logDelegate("Param count mismatch. Expected " + pList.Length.ToString() + " got " + sParams.Length);

                hasSucceded = false;
            }
            else
            {
                for (int i = 0; i < pList.Length; i++)
                {
                    var res = TryGetTypeFromString(pList[i].ParameterType, sParams[i]);

                    if (res == null)
                        hasSucceded = false;

                    out_params[i] = res;
                }
            }

            return hasSucceded;
        }

        public static bool ParamStringToObjects(string s, ParameterInfo[] pList, out object[] out_params)
        {
            var sParams = ParamStringToElements(s);
            return StringArrayToObjects(sParams, pList, out out_params);
        }

        public static object TryGetTypeFromString(System.Type t, string s)
        {
            object retval = null;

            try
            {
                retval = Convert(t, s);
            }
            catch (System.Exception)
            {
                logDelegate("Failed to convert param. Got " + s + " could not convert to " + t.Name);
            }

            return retval;
        }

        public static bool Supports(Type pType)
        {
            return converters.ContainsKey(pType);
        }

        public static bool Supports(ParameterInfo[] pInfo)
        {
            for (int i = 0; i < pInfo.Length; i++)
            {
                if (!Supports(pInfo[i].ParameterType)) return false;
            }

            return true;
        }
    }
}