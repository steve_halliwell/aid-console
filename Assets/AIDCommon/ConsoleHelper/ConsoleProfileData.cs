﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AID
{
    [CreateAssetMenu()]
    public class ConsoleProfileData : FancyScriptableObject<ConsoleProfileData>
    {

        public List<TextAsset> textAssetProfiles;

        //[CUDLR.Command("Console.LoadProfile", "Attempts to load one of the preset console profiles, will add all containing commands to the console.")]
        //public static void LoadConsoleProfile(string profileName)
        //{
        //    var inst = GetFirstInstance();
        //    if (inst != null)
        //    {
        //        var res = inst.textAssetProfiles.Find(x => x.name == profileName);

        //        if (res != null)
        //        {
        //            List<AID.ConsoleHelper.ClassByNameData> fromCSV = DeadSimpleCSV.ConvertStringCSVToObjects<AID.ConsoleHelper.ClassByNameData>(res.text, true);
        //            ConsoleHelper.AddAllStaticsByClassName(fromCSV);
        //        }
        //    }
        //}
    }
}