﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using CUDLR;
using System.Net.Sockets;
using System.Net;

namespace AID
{
    /*
        Functions to assist with adding and using the CUDLR Console.
    */
    public class ConsoleHelper
    {
		public const BindingFlags PublicStatic = BindingFlags.Static | BindingFlags.Public | BindingFlags.DeclaredOnly;
        public const BindingFlags PublicInstance = BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly;

        private static void AddCommand(string name, ICustomAttributeProvider item, CommandTree.CommandData.Callback wrappedFunc)
        {
            var cudlrAttrList = item.GetCustomAttributes(typeof(CUDLR.CommandAttribute), false);
            var cudlrAttr = (cudlrAttrList != null && cudlrAttrList.Length > 0) ? (CommandAttribute)cudlrAttrList[0] : null;
            bool mainThread = cudlrAttr != null ? cudlrAttr.m_runOnMainThread : true;
            var desc = cudlrAttr != null ? cudlrAttr.m_help : "";
            
            CUDLR.Console.RegisterCommand(name, desc, wrappedFunc, mainThread);
		}

        public static void AddAllMethodsToConsole(object instance, string startingName, System.Type t, BindingFlags bindingFlags, bool dontAddMethodsWithCommandAttribute)
        {
            var smList = t.GetMethods(bindingFlags)
                .Where(m => !m.IsSpecialName);

            foreach (var item in smList)
            {
                var attrs = item.GetCustomAttributes(typeof(CUDLR.CommandAttribute), false);
                if (dontAddMethodsWithCommandAttribute && attrs != null && attrs.Length > 0)
                    continue;

                attrs = item.GetCustomAttributes(typeof(CommandIgnore), false);
                if (attrs != null && attrs.Length > 0)
                    continue;

                AddMethodToConsole(item, instance, startingName);
            }
        }

		public static void AddAllPropsToConsole(object instance, string startingName, Type type, BindingFlags bindingFlags)
        {
			var spList = type.GetProperties(bindingFlags);

            foreach (var item in spList)
            {
                var attrs = item.GetCustomAttributes(typeof(CommandIgnore), false);
                if (attrs != null && attrs.Length > 0)
                    continue;

                PropAndFieldInternalHelper(instance, null, item, startingName);
            }
        }

		public static void AddAllFieldsToConsole(object instance, string startingName, Type type, BindingFlags bindingFlags)
        {
			var sfList = type.GetFields(bindingFlags);

            foreach (var item in sfList)
            {
                var attrs = item.GetCustomAttributes(typeof(CommandIgnore), false);
                if (attrs != null && attrs.Length > 0)
                    continue;

                PropAndFieldInternalHelper(instance, item, null, startingName);
            }
        }

		public static void AddAllStaticsToConsole(Type type, string startingName = null, BindingFlags bindingFlags = PublicStatic, bool shouldAddMethods = true, bool shouldAddFields = true, bool shouldAddProps = true)
		{
            if (string.IsNullOrEmpty(startingName))
                startingName = type.ToString();

			AddAllToConsole(null, startingName, type, bindingFlags,shouldAddMethods, shouldAddFields, shouldAddProps);
		}

        //will deduce type if null
		public static void AddAllToConsole(object instance, string startingName, Type type = null, BindingFlags bindingFlags = PublicInstance, bool shouldAddMethods = true, bool shouldAddFields = true, bool shouldAddProps = true, bool suppressAutoAddOfCommandTaggedMethods = true)
        {
            if (instance == null && type == null)
                return;

            if (type == null && instance != null)
                type = instance.GetType();

            var attrs = type.GetCustomAttributes(typeof(CommandIgnore), false);
            if (attrs != null && attrs.Length > 0)
                return;

			if (shouldAddMethods) AddAllMethodsToConsole(instance, startingName, type, bindingFlags, suppressAutoAddOfCommandTaggedMethods);
			if(shouldAddFields) AddAllFieldsToConsole(instance, startingName, type, bindingFlags);
			if(shouldAddProps) AddAllPropsToConsole(instance, startingName, type, bindingFlags);
        }

        public static bool AddMethodToConsole(MethodInfo item, object instance, string startingName)
        {
            var wrappedFunc = CallbackFromMethod(item, instance);

            if (wrappedFunc == null)
                return false;

            ConsoleHelper.AddCommand(startingName + "." + item.Name, item, wrappedFunc);
            return true;
        }

        public static CommandTree.CommandData.Callback CallbackFromMethod(MethodInfo item, object instance)
        {
            var pList = item.GetParameters();

            if (!StringToType.Supports(pList))
                return null;

            //we could probably optimise this if it just takes a string but what would the point be 
            return (string stringIn) =>
                {
                    object[] parameters;

                    var prevLog = StringToType.logDelegate;
                    StringToType.logDelegate = CUDLR.Console.Log;

                    if (StringToType.ParamStringToObjects(stringIn, pList, out parameters))
                    {
                        item.Invoke(instance, parameters);
                    }
                    else
                    {
                        CUDLR.Console.Log("Could not call " + item.Name);
                    }


                    StringToType.logDelegate = prevLog;
                };
            
        }

        //wrap the functionality that is DAMN NEAR identical for fields and properties so we don't have to maintain two versions in 2 locations
        private static void PropAndFieldInternalHelper(object instance, FieldInfo field, PropertyInfo property, string startingName)
        {
            Type paramType = field != null ? field.FieldType : property.PropertyType;

            if (!StringToType.Supports(paramType))
                return;

            //fancy wrapper goes here that returns value safely on no params and tries to convert on 1 param
            CommandTree.CommandData.Callback wrappedFunc = (string stringIn) =>
            {
                if (string.IsNullOrEmpty(stringIn))
                {
                    //use it as a get
                    if (field != null)
                    {
							CUDLR.Console.Log("=" + field.GetValue(instance).ToString());
                    }
                    else
                    {
                        if(property.CanRead)
								CUDLR.Console.Log("=" + property.GetValue(instance,null).ToString());
                        else
                            CUDLR.Console.Log("Not allowed to read property");
                    }
                return;
                }

                var prevLog = StringToType.logDelegate;
                StringToType.logDelegate = CUDLR.Console.Log;

                object parameter = StringToType.TryGetTypeFromString(paramType, stringIn);

                if (parameter != null)
                {
                    //use it as a set
                    if (field != null)
                    {
							field.SetValue(instance, parameter);
                    }
                    else
                    {
                        if(property.CanWrite)
								property.SetValue(instance, parameter, null);
                        else
                            CUDLR.Console.Log("Not allowed to write property");
                    }
                }
                
                StringToType.logDelegate = prevLog;
            };

            if(field != null)
                ConsoleHelper.AddCommand(startingName + "." + field.Name, field, wrappedFunc);
            else
                ConsoleHelper.AddCommand(startingName + "." + property.Name, property, wrappedFunc);

        }

        public static string GetExternalIP()
        {
            string localIP = "127.0.0.1";
			try
			{
				using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
				{
					socket.Connect("8.8.8.8", 65530);
					IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
					localIP = endPoint.Address.ToString();
				}
			}
			catch (System.Exception ex)
			{
				
			}
            return localIP;
        }

        [Flags]
		public enum PartsToBind
		{
			//0 is none
			Methods = (1 << 0),
			Props 	= (1 << 1),
			Fields 	= (1 << 2),
		}

        //typenames and namesinconsole must match length
		[System.Serializable]
		public class ClassByNameData
		{
            public ClassByNameData() { }
            public ClassByNameData(string TypeName, string NameInConsole = null)
            {
                typeName = TypeName;
                nameInConsole = NameInConsole;
            }

            public string typeName = string.Empty;
			public string nameInConsole = null;
			public BindingFlags bindingFlags = PublicStatic;
			public PartsToBind partsToBind = PartsToBind.Methods | PartsToBind.Fields | PartsToBind.Props;
		}
        
        public static void AddAllStaticsByClassName(List<ClassByNameData> classesByName)
        {
            var assms = System.AppDomain.CurrentDomain.GetAssemblies();

            for (int i = 0; i < classesByName.Count; i++)
            {
                var t = FindTypeByNameInAllAssemblies(classesByName[i].typeName, assms);

                if(t != null)
                {
                    AddAllStaticsToConsole(t, classesByName[i].nameInConsole, classesByName[i].bindingFlags, 
                        (classesByName[i].partsToBind & PartsToBind.Methods) != 0,
                        (classesByName[i].partsToBind & PartsToBind.Fields) != 0,
                        (classesByName[i].partsToBind & PartsToBind.Props) != 0);
                }
            }
        }

        public static Type FindTypeByNameInAllAssemblies(string typeName, Assembly[] assms = null)
        {
            if(assms == null)
                assms = System.AppDomain.CurrentDomain.GetAssemblies();

            System.Type t = null;
            System.Reflection.Assembly assm = null;

            //find first match of type in any of the used assemblies
            for (int i = 0; i < assms.Length && t == null; i++)
            {
                assm = assms[i];
                t = assm.GetType(typeName, false, true);
            }

            return t;
        }
        
    }

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class CommandIgnore : Attribute
    {
    }
}