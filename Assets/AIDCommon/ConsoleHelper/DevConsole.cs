﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace AID
{
    //adapted from https://github.com/Wenzil/UnityConsole
    public class DevConsole : MonoBehaviour
    {
        public UnityEngine.UI.Text outPutText;
        public GameObject outputTextLocalRoot;
        public CanvasGroup outputTextcanvasGroup;
        public int logsToShow = 5;
        public bool firstLineOnly = true;
        private List<int> mainConsoleLogLengths = new List<int>();
        private object logLock = new object();

        [Tooltip("Negative is infinite")]
        public float showLogFor = -1;
        private int lastTweenID = -1;
        public KeyCode toggleConsole, confirmCommandEntered, suggestionKey;
        public UnityEngine.UI.Text previewText;
        public CanvasGroup previewTextCanvasGroup;
        public UnityEngine.UI.InputField inputField;

        private string[] prevCompleteResults;
        private int prevCompleteIndex;
        private string previousInputLine;


        static readonly Dictionary<LogType, string> logTypeColors = new Dictionary<LogType, string>
        {
            { LogType.Assert, "<color=#ffffffff>" },
            { LogType.Error, "<color=#ff0000ff>" },
            { LogType.Exception, "<color=#ff0000ff>" },
            { LogType.Warning, "<color=#ffff00ff>" },
        };


        private void PreviewTextHandleLog(string condition, string stackTrace, LogType type)
        {
            if (!enabled)
                return;

            lock (logLock)
            {
                //if (mainConsoleLogLengths.Count >= logsToShow)
                //{
                //    //remove from front
                //    outPutText.text = outPutText.text.Substring(mainConsoleLogLengths[0]);
                //    mainConsoleLogLengths.PopFront();

                //mainConsoleLogLengths.Add(toLog.Length);
                //outPutText.text += toLog;
                //}

                if (firstLineOnly)
                {
                    var p = condition.IndexOf("\n");
                    if (p != -1)
                    {
                        condition = condition.Substring(0, p);
                    }
                }

                string colPrefix = string.Empty;
                string toLog = string.Empty;

                if (logTypeColors.TryGetValue(type, out colPrefix))
                {
                    toLog = logTypeColors[type] + condition + "</color>\n";
                }
                else
                {
                    toLog = condition;
                }

                AddPreviewText(toLog);
            }
        }

        public void AddPreviewText(string toLog)
        {
            if (toLog.Back() != '\n')
                toLog += "\n";


            if (showLogFor > 0)
            {
                previewText.text += toLog;
                var thisLogLen = toLog.Length;

                //handle removing this from the preview after time
                LeanTween.value(1, 0, 0.25f)
                    .setDelay(showLogFor)
                    .setOnComplete(() =>
                   {
                       previewText.text = previewText.text.Substring(thisLogLen);
                   });

                //handle the fade out
                if (lastTweenID >= 0)
                    LeanTween.cancel(lastTweenID);

                previewTextCanvasGroup.alpha = 1;

                lastTweenID = LeanTween.value(1, 0, 0.25f)
                    .setDelay(showLogFor)
                    .setOnUpdate((f) =>
                    {
                        previewTextCanvasGroup.alpha = f;
                    })
                    .setOnComplete(() =>
                    {
                        lastTweenID = 0;
                    })
                    .id;
            }
        }

        public void AddToMainOutput(string toLog)
        {
            lock (logLock)
            {
                if (toLog.Back() != '\n')
                    toLog += "\n";

                if (mainConsoleLogLengths.Count >= logsToShow)
                {
                    //remove from front
                    outPutText.text = outPutText.text.Substring(mainConsoleLogLengths[0]);
                    mainConsoleLogLengths.PopFront();
                }

                mainConsoleLogLengths.Add(toLog.Length);
                outPutText.text += toLog;
            }
        }

        public void Awake()
        {
            ////add all default stuff
            //ConsoleHelper.AddAllStaticsToConsole(typeof(Physics));
            //ConsoleHelper.AddAllStaticsToConsole(typeof(Physics2D));
            //ConsoleHelper.AddAllStaticsToConsole(typeof(Time));
            //ConsoleHelper.AddAllStaticsToConsole(typeof(QualitySettings));
            //ConsoleHelper.AddAllStaticsToConsole(typeof(Graphics));
            //ConsoleHelper.AddAllStaticsToConsole(typeof(Renderer));
            //ConsoleHelper.AddAllStaticsToConsole(typeof(RenderSettings));
            //ConsoleHelper.AddAllStaticsToConsole(typeof(Application));
            //ConsoleHelper.AddAllStaticsToConsole(typeof(Screen));
            //ConsoleHelper.AddAllStaticsToConsole(typeof(CUDLR.Console));
        }

        void OnEnable()
        {
            if (outPutText == null)
                outPutText = GetComponent<UnityEngine.UI.Text>();

            outPutText.text = "";
            mainConsoleLogLengths.Clear();

            Application.logMessageReceived += PreviewTextHandleLog;
            CUDLR.Console.OnOutputUpdated += HandleCUDLROutput;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= PreviewTextHandleLog;
            CUDLR.Console.OnOutputUpdated -= HandleCUDLROutput;
        }

        private void HandleCUDLROutput(string addedString)
        {
            AddToMainOutput(addedString);
        }

        void Update()
        {
            if (Input.GetKeyDown(toggleConsole))
                ToggleConsole();

            if (inputField.gameObject.activeInHierarchy)
            {
                if (Input.GetKeyDown(confirmCommandEntered))
                {
                    OnConsoleInput(inputField.text);
                }

                if (Input.GetKeyDown(suggestionKey))
                {
                    CompleteConsoleInput();
                }
            }

            if(previousInputLine != inputField.text)
            {
                previousInputLine = inputField.text;

                prevCompleteResults = null;
                prevCompleteIndex = 0;
            }
        }

        public void ToggleConsole()
        {
            outputTextLocalRoot.SetActive(!outputTextLocalRoot.activeInHierarchy);
            previewText.gameObject.SetActive(!outputTextLocalRoot.activeInHierarchy);

            if (outputTextLocalRoot.activeInHierarchy)
            {
                if (inputField.text.Back() == '`')
                {
                    inputField.text = inputField.text.RemoveBack();
                }

                inputField.Select();
                inputField.ActivateInputField();
            }
        }

        public void OnConsoleInput(string input)
        {
            //test pushing input string into CUDLR
            CUDLR.CommandTree cmd;
            if (CUDLR.Console.Run(input, out cmd))
            {
                inputField.Select();
                inputField.ActivateInputField();
            }
            else
            {
                //failed to call, if it was a containing command node run complete
                if (cmd != null && cmd.Command.callback == null && cmd.NumSubComands > 0)
                {
                    CompleteConsoleInput();
                }
            }
        }

        public void CompleteConsoleInput()
        {
            //first time trying to complete
            if (prevCompleteResults == null || prevCompleteResults.Length == 0)
            {
                prevCompleteResults = CUDLR.Console.Complete(inputField.text.Substring(0,inputField.selectionAnchorPosition));
                prevCompleteIndex = 0;

                if (prevCompleteResults.Length == 1)
                {
                    inputField.text = prevCompleteResults[0];
                    inputField.MoveTextEnd(false);

                    //lets assume we just autocompleted and this might be a holder object so try to complete again
                    //  won't run endlessly as if it is the same as the current input then we don't recurse
                    prevCompleteResults = null;
                    CompleteConsoleInput();
                }
                else if (prevCompleteResults.Length > 0)
                {
                    int firstCommonChars = FirstCommonCharacters(prevCompleteResults, inputField.selectionAnchorPosition);

                    inputField.text = prevCompleteResults[0].Substring(0, firstCommonChars);
                    inputField.MoveTextEnd(false);

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < prevCompleteResults.Length; i++)
                    {
                        sb.Append('\n');

                        sb.Append(prevCompleteResults[i]);
                    }

                    AddToMainOutput(sb.ToString());
                    AutoCompletePreviewText();
                }
            }
            else
            {
                //trying to tab thro commands
                prevCompleteIndex++;
                prevCompleteIndex %= prevCompleteResults.Length;
                AutoCompletePreviewText();
            }
            previousInputLine = inputField.text;
        }

        private void AutoCompletePreviewText()
        {
            inputField.text = prevCompleteResults[prevCompleteIndex];
            inputField.selectionFocusPosition = inputField.text.Length;
        }

        public static int FirstCommonCharacters(string[] strs, int startingIndex = 0)
        {
            var shortest = strs[0].Length;

            //find the actual shortest
            for (int i = 1; i < strs.Length; i++)
            {
                if (strs[i].Length < shortest)
                    shortest = strs[i].Length;
            }

            //find where they stop matching
            for (; startingIndex < shortest; startingIndex++)
            {
                var targetChar = strs[0][startingIndex];
                for (int i = 1; i < strs.Length; i++)
                {
                    if (targetChar != strs[i][startingIndex])
                        return startingIndex;
                }
            }

            //never stopped matching so return what will be the shortest index
            return startingIndex;
        }
    }
}